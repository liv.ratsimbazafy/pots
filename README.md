## POTS
Events based application build with ReactJS & Firebase

## Requirements
node -v > 10.0
npm -v > 4.0

## Installation
- Clone the repository
```bash
npm install && npm start
```

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.


## Version
MVP


## License 
MIT
